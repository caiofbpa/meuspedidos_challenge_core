import re


class FormData:
    def __init__(self, form, presenter):
        self.form_data = form
        self.presenter = presenter

    def get(self, key):
        return self.form_data.get(key)

    def dict(self):
        return self.form_data

    def is_valid(self):
        valid_name = self.is_name_valid()
        valid_email = self.is_email_valid()
        valid_marks = self.are_marks_valid()
        return valid_name and valid_email and valid_marks

    def is_name_valid(self):
        if self.form_data.get('name'):
            return True
        else:
            self.presenter.present_error(RegisterApplication.NAME_MISSING)
            return False

    def is_email_valid(self):
        if self.form_data.get('email'):
            if re.match(r"[^@]+@[^@]+\.[^@]+", self.form_data.get('email')):
                return True
            else:
                self.presenter.present_error(RegisterApplication.EMAIL_INVALID)
        else:
            self.presenter.present_error(RegisterApplication.EMAIL_MISSING)
        return False

    def are_marks_valid(self):
        all_marks_valid = True
        for skill in ['html', 'css', 'js', 'python', 'django', 'ios', 'android']:
            all_marks_valid = self.is_mark_valid(skill) and all_marks_valid
        return all_marks_valid

    def is_mark_valid(self, skill):
        if(self.form_data.has_key(skill)):
            self.form_data[skill] = self.parse_int(self.form_data.get(skill))
        mark = self.form_data.get(skill)
        if 0 <= mark <= 10:
            return True
        else:
            self.presenter.present_error(skill + '.out_of_bounds')
            return False

    def parse_int(self, string):
        if not string:
            return 0
        try:
            return int(string)
        except ValueError:
            return -1


class RegisterApplication:
    NAME_MISSING = 'name.missing'
    EMAIL_MISSING = 'email.missing'
    EMAIL_INVALID = 'email.invalid'

    def __init__(self, presenter, mailer, persistence):
        self.presenter = presenter
        self.mailer = mailer
        self.persistence = persistence

    def register(self, form_data):
        self.form_data = FormData(form_data, self.presenter)
        if self.form_data.is_valid():
            self.persistence.save(self.form_data.dict())
            if self.is_frontend():
                self.mailer.send_frontend_email(self.form_data.get('email'))
            if self.is_backend():
                self.mailer.send_backend_email(self.form_data.get('email'))
            if self.is_mobile():
                self.mailer.send_mobile_email(self.form_data.get('email'))
            if not self.is_anything():
                self.mailer.send_generic_email(self.form_data.get('email'))

    def is_frontend(self):
        return self.form_data.get('html') >= 7 and self.form_data.get('css') >= 7 and self.form_data.get('js') >= 7

    def is_backend(self):
        return self.form_data.get('python') >= 7 and self.form_data.get('django') >= 7

    def is_mobile(self):
        return self.form_data.get('ios') >= 7 or self.form_data.get('android') >= 7

    def is_anything(self):
        return self.is_frontend() or self.is_backend() or self.is_mobile()