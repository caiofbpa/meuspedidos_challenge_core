class PresenterSpy:
    def __init__(self):
        self.errors = []

    def present_error(self, error):
        self.errors.append(error)

    def get_errors(self):
        return self.errors


class MailerSpy:
    def __init__(self):
        self.emails = []

    def send_generic_email(self, to):
        self.emails.append({'to': to, 'type': 'generic'})

    def send_frontend_email(self, to):
        self.emails.append({'to': to, 'type': 'frontend'})

    def send_backend_email(self, to):
        self.emails.append({'to': to, 'type': 'backend'})

    def send_mobile_email(self, to):
        self.emails.append({'to': to, 'type': 'mobile'})

    def get_emails(self):
        return self.emails
        
class PersistenceSpy:
    def __init__(self):
        self.application = None
        
    def save(self, application):
        self.application = application

    def get_application(self):
        return self.application