import unittest
from register_application import RegisterApplication
from test_doubles import PresenterSpy, MailerSpy, PersistenceSpy


class TestRegisterApplication(unittest.TestCase):
    NAME = 'Cassia the Candidate'
    EMAIL = 'cassia@example.com'

    def setUp(self):
        self.presenter = PresenterSpy()
        self.mailer = MailerSpy()
        self.persistence = PersistenceSpy()
        self.use_case = RegisterApplication(self.presenter, self.mailer, self.persistence)

    def test_missing_name_and_email(self):
        self.use_case.register({'name': '', 'email': '',
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 0})
        self.assertEqual([RegisterApplication.NAME_MISSING, RegisterApplication.EMAIL_MISSING],
                         self.presenter.get_errors())
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_missing_email(self):
        self.use_case.register({'name': self.NAME, 'email': '',
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 0})
        self.assert_error(RegisterApplication.EMAIL_MISSING)
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_missing_name(self):
        self.use_case.register({'name': '', 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 0})
        self.assert_error(RegisterApplication.NAME_MISSING)
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_invalid_email(self):
        self.use_case.register({'name': self.NAME, 'email': 'invalid email',
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 0})
        self.assert_error(RegisterApplication.EMAIL_INVALID)
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_generic(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 0}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assert_sent_email('generic')
        self.assert_saved_application(application)

    def test_frontend(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 10, 'css': 10, 'js': 10, 'python': 0, 'django': 0, 'ios': 0, 'android': 0}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assert_sent_email('frontend')
        self.assert_saved_application(application)

    def test_backend(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 10, 'django': 10, 'ios': 0, 'android': 0}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assert_sent_email('backend')
        self.assert_saved_application(application)

    def test_mobile_ios(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 10, 'android': 0}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assert_sent_email('mobile')
        self.assert_saved_application(application)

    def test_mobile_android(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 10}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assert_sent_email('mobile')
        self.assert_saved_application(application)

    def test_negative_android(self):
        self.use_case.register({'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': -1})
        self.assert_error('android.out_of_bounds')
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_superhuman_android(self):
        self.use_case.register({'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 0, 'android': 11})
        self.assert_error('android.out_of_bounds')
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_superhuman_android_and_ios(self):
        self.use_case.register({'name': self.NAME, 'email': self.EMAIL,
                                'html': 0, 'css': 0, 'js': 0, 'python': 0, 'django': 0, 'ios': 11, 'android': 11})
        self.assertEqual(['ios.out_of_bounds', 'android.out_of_bounds'],
                         self.presenter.get_errors())
        self.assert_no_sent_emails()
        self.assert_no_saved_applications()

    def test_complete_candidate(self):
        application = {'name': self.NAME, 'email': self.EMAIL,
                                'html': 10, 'css': 10, 'js': 10, 'python': 10, 'django': 10, 'ios': 10, 'android': 10}
        self.use_case.register(application)
        self.assert_no_errors()
        self.assertEqual([{'to': self.EMAIL, 'type': 'frontend'},
                          {'to': self.EMAIL, 'type': 'backend'},
                          {'to': self.EMAIL, 'type': 'mobile'}], self.mailer.get_emails())
        self.assert_saved_application(application)

    def assert_no_sent_emails(self):
        self.assertEqual([], self.mailer.get_emails())

    def assert_no_errors(self):
        self.assertEqual([], self.presenter.get_errors())

    def assert_error(self, error):
        self.assertEqual([error], self.presenter.get_errors())

    def assert_sent_email(self, email_type):
        self.assertEqual([{'to': self.EMAIL, 'type': email_type}], self.mailer.get_emails())

    def assert_saved_application(self, application):
        self.assertEqual(application, self.persistence.get_application())

    def assert_no_saved_applications(self):
        self.assertIsNone(self.persistence.get_application())


if __name__ == '__main__':
    unittest.main()